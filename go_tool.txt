explicitly compile and link go source file
==
from a folder where you have your main.go file, run
$ go tool compile -S main.go > main.s
$ go tool asm main.s
$ go tool link -o YOU_BINARY_NAME main.o
$ ./YOU_BINARY_NAME


see go assembler source code
==
$ go tool compile -S main.go
$ cat main.s

$ go tool compile main.go
$ cat main.o


read compiled object file
==
go tool objdump -S main.o